FROM node:18 AS build

RUN apt update && apt install -y htop git curl wget

WORKDIR /usr/src/app

COPY container ./

CMD [ "node", "shim/index.js" ]

