import { loadavg, cpus } from "node:os";
import { promisify } from "node:util";
import { exec as CpExec } from "node:child_process";

const exec = promisify(CpExec);

getDiskStats();

getCPUStats();

async function getDiskStats(){
  let { stdout: resultMB } = await exec("df -B MB /proc/meminfo");
  resultMB = resultMB.replace('/proc/meminfo', '/')
  const valuesMB = resultMB
    .trim()
    .split("\n")[1]
    .split(/\s+/)
    .map((res) => Number(res.replace("MB", "")));
  const totalDiskMB = valuesMB[1];
  const usedDiskMB = valuesMB[2];
  const availableDiskMB = valuesMB[3];
  console.log(resultMB)
  console.log(totalDiskMB, usedDiskMB, availableDiskMB)
}

async function getCPUStats() {
  let cmd = "lscpu"
  let numCPUs = cpus().length;
  if(numCPUs < 6){
      numCPUs = 6
      cmd = "cat /var/lib/wawu/lscpu"
  }
  const { stdout: result } = await exec(cmd);
  const loadAvgs = loadavg();
  
  console.log(numCPUs);
  console.log(loadAvgs);
  console.log(result);
}



